import React, {Fragment, useEffect, useState} from 'react';
import { serializePackageJson } from './utils';

import './App.css';

function App() {
  const [list, setList] = useState({});
  const { dependencies } = list;
  const [isSolved, setSolve] = useState(false);
  const formattedList = isSolved ? serializePackageJson(dependencies) : dependencies;

  useEffect(() => {
    fetch('https://raw.githubusercontent.com/guestyorg/candidate-task/master/complex-package.json')
      .then(response => response.json())
      .then(response => { setList(response) }
    );
  }, []);

  const renderList = (list) => {
    return (
      <ul>
        {Object.entries(list).map(([name, value]) => {
          const version = value instanceof Object ? value.version : value;
          const elementClass = value.dependencies === undefined ? 'without-dependencies' : '';

          return (
            <Fragment key={name}>
              <li className={elementClass}>{name} {version} <span className="caret" /></li>
              {value.dependencies && renderList(value.dependencies)}
            </Fragment>
          );
        })}
      </ul>
    );
  };

  const handlerSwitch = () => {
    setSolve(!isSolved);
  };

  if (!formattedList) {
    return <p>Empty</p>;
  }

  return (
    <div className="App">
      <button onClick={handlerSwitch}>{isSolved ? 'Unsolved' : 'Solved'}</button>
      <ul>
        <li>{list.name}</li>
        {renderList(formattedList)}
      </ul>
    </div>
  );
}

export default App;
