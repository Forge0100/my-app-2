export const serializePackageJson = (json, result = {}) => {
  Object.entries(json).forEach(([name, value]) => {
    if (name in result) {
      return;
    }

    result[name] = value.version || value;

    if (value instanceof Object) {
      serializePackageJson(value.dependencies, result);
    }
  });

  return result;
};
